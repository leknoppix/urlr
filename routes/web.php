<?php

use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\UrlController as AdminUrlController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\UrlController;
use Illuminate\Support\Facades\Route;

Route::get('/', [UrlController::class, 'index'])->name('url.index');

Route::get('/installstorage', function() {
    Artisan::call('storage:link');
    return redirect('/login');
});

Route::get('administration/dashboard', [DashboardController::class, '__invoke'])->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('administration/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('administration/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('administration/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

});

Route::prefix('administration')->name('administration.')->middleware('auth')->group(function () {
    Route::resource('links', AdminUrlController::class)->parameters(['links' => 'url']);
    Route::get('download/qrcode/{url}', [AdminUrlController::class, 'downloadQrCode'])->name('links.download');
    Route::resource('users', App\Http\Controllers\Admin\UserController::class)->except('show');
});

require __DIR__ . '/auth.php';

Route::get('{slug}', [UrlController::class, 'redirection'])->name('url.redirection');
