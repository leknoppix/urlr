<?php

return [
    // Url de redirection de base si aucun paramètre 'url' n'est transmise
    'url_redirect' => env('APP_URL_REDIRECT', 'https://google.fr'),
];
