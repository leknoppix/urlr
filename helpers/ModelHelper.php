<?php

// @formatter:off
// phpcs:ignoreFile
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */

namespace App\Models{
    /**
     * @property int $id
     * @property int $url_id
     * @property string $click_date
     * @property int $click_count
     * @property \Illuminate\Support\Carbon|null $created_at
     * @property \Illuminate\Support\Carbon|null $updated_at
     * @property-read \App\Models\Url $url
     *
     * @method static \Database\Factories\ClickFactory factory($count = null, $state = [])
     * @method static \Illuminate\Database\Eloquent\Builder|Click newModelQuery()
     * @method static \Illuminate\Database\Eloquent\Builder|Click newQuery()
     * @method static \Illuminate\Database\Eloquent\Builder|Click query()
     * @method static \Illuminate\Database\Eloquent\Builder|Click whereClickCount($value)
     * @method static \Illuminate\Database\Eloquent\Builder|Click whereClickDate($value)
     * @method static \Illuminate\Database\Eloquent\Builder|Click whereCreatedAt($value)
     * @method static \Illuminate\Database\Eloquent\Builder|Click whereId($value)
     * @method static \Illuminate\Database\Eloquent\Builder|Click whereUpdatedAt($value)
     * @method static \Illuminate\Database\Eloquent\Builder|Click whereUrlId($value)
     *
     * @mixin \Eloquent
     */
    #[\AllowDynamicProperties]
    class IdeHelperClick
    {
    }
}

namespace App\Models{
    /**
     * @property int $id
     * @property string $name
     * @property string $slug
     * @property string $url
     * @property int $user_id
     * @property string|null $fileurl
     * @property \Illuminate\Support\Carbon|null $created_at
     * @property \Illuminate\Support\Carbon|null $updated_at
     * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Click> $clicks
     * @property-read int|null $clicks_count
     * @property-read mixed $created_at_formatted
     * @property-read mixed $totalclicks
     * @property-read \App\Models\User|null $user
     *
     * @method static \Database\Factories\UrlFactory factory($count = null, $state = [])
     * @method static \Illuminate\Database\Eloquent\Builder|Url newModelQuery()
     * @method static \Illuminate\Database\Eloquent\Builder|Url newQuery()
     * @method static \Illuminate\Database\Eloquent\Builder|Url query()
     * @method static \Illuminate\Database\Eloquent\Builder|Url whereCreatedAt($value)
     * @method static \Illuminate\Database\Eloquent\Builder|Url whereFileurl($value)
     * @method static \Illuminate\Database\Eloquent\Builder|Url whereId($value)
     * @method static \Illuminate\Database\Eloquent\Builder|Url whereName($value)
     * @method static \Illuminate\Database\Eloquent\Builder|Url whereSlug($value)
     * @method static \Illuminate\Database\Eloquent\Builder|Url whereUpdatedAt($value)
     * @method static \Illuminate\Database\Eloquent\Builder|Url whereUrl($value)
     * @method static \Illuminate\Database\Eloquent\Builder|Url whereUserId($value)
     *
     * @mixin \Eloquent
     */
    #[\AllowDynamicProperties]
    class IdeHelperUrl
    {
    }
}

namespace App\Models{
    /**
     * @property int $id
     * @property string $name
     * @property string|null $surname
     * @property string $email
     * @property \Illuminate\Support\Carbon|null $email_verified_at
     * @property mixed $password
     * @property string|null $remember_token
     * @property \Illuminate\Support\Carbon|null $created_at
     * @property \Illuminate\Support\Carbon|null $updated_at
     * @property-read mixed $created_at_formatted
     * @property-read \Illuminate\Notifications\DatabaseNotificationCollection<int, \Illuminate\Notifications\DatabaseNotification> $notifications
     * @property-read int|null $notifications_count
     * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Url> $urls
     * @property-read int|null $urls_count
     *
     * @method static \Database\Factories\UserFactory factory($count = null, $state = [])
     * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
     * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
     * @method static \Illuminate\Database\Eloquent\Builder|User query()
     * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
     * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
     * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
     * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
     * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
     * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
     * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
     * @method static \Illuminate\Database\Eloquent\Builder|User whereSurname($value)
     * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
     *
     * @mixin \Eloquent
     */
    #[\AllowDynamicProperties]
    class IdeHelperUser
    {
    }
}
