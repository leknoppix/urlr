<?php

namespace Tests\Unit\Models;

use App\Models\Click;
use App\Models\Url;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UrlTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function user_relation()
    {
        $user = User::factory()->create();
        $url = Url::factory()->create(['user_id' => $user->id]);

        $this->assertInstanceOf(User::class, $url->user);
        $this->assertEquals($user->id, $url->user->id);
    }

    /**
     * @test
     */
    public function clicks_relation()
    {
        $user = User::factory()->create();
        $url = Url::factory()->create(['user_id' => $user->id]);
        $clicks = Click::factory()->count(3)->create(['url_id' => $url->id]);
        $this->assertInstanceOf(Click::class, $url->clicks->first());
        $this->assertCount(3, $url->clicks);
    }

    /**
     * @test
     */
    public function url_creation()
    {
        $user = User::factory()->create();
        $urlData = [
            'name' => 'monlien',
            'slug' => 'test-slug',
            'url' => 'https://example.com',
            'user_id' => $user->id,
        ];

        $url = Url::create($urlData);
        $this->assertInstanceOf(Url::class, $url);
        $this->assertEquals($urlData['slug'], $url->slug);
        $this->assertEquals($urlData['url'], $url->url);
    }
}
