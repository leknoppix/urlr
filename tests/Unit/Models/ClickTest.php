<?php

namespace Tests\Unit\Models;

use App\Models\Click;
use App\Models\Url;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ClickTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function url_relation()
    {
        $user = User::factory()->create();
        $url = Url::factory()->create();
        $click = Click::factory()->create(['url_id' => $url->id]);
        $this->assertInstanceOf(Url::class, $click->url);
        $this->assertEquals($url->id, $click->url->id);
    }

    /**
     * @test
     */
    public function click_creation()
    {
        $user = User::factory()->create();
        $url = Url::factory()->create();
        $nbclic = rand(1, 99);
        $clickData = [
            'url_id' => $url->id,
            'click_count' => $nbclic,
            'click_date' => date('Y-d-m', time()),
        ];
        $click = Click::create($clickData);
        $click2Data = [
            'url_id' => $url->id,
            'click_count' => $nbclic - 1,
            'click_date' => date('Y-d-m', time() - 86400),
        ];
        $click2 = Click::create($click2Data);

        $this->assertInstanceOf(Click::class, $click);
        $this->assertEquals($clickData['url_id'], $click->url_id);
        $this->assertEquals($clickData['click_count'], $click->click_count);
        $this->assertEquals(Click::count(), 2);
    }
}
