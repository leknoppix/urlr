<?php

namespace Tests\Unit\Models;

use App\Models\Url;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function urls_relation()
    {
        $user = User::factory()->create();
        $urls = Url::factory()->count(3)->create(['user_id' => $user->id]);

        $this->assertInstanceOf(Url::class, $user->urls->first());
        $this->assertCount(3, $user->urls);
    }
}
