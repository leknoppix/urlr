<?php

namespace Tests\Unit\Http\Controllers;

use App\Http\Controllers\UrlController;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Config;
use Tests\TestCase;

class UrlControllerTest extends TestCase
{
    /**
     * @test
     */
    public function url_Index()
    {
        Config::set('url.url_redirect', 'http://example.com');

        $urlController = new UrlController;
        $response = $urlController->index();

        $this->assertInstanceOf(RedirectResponse::class, $response);
        $this->assertEquals('http://example.com', $response->getTargetUrl());
    }
}
