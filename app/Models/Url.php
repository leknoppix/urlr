<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @mixin IdeHelperUrl
 */
class Url extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $fillable = [
        'name',
        'url',
        'slug',
        'fileurl',
        'user_id'
    ];

    protected $appends = ['totalclicks', 'created_at_formatted'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function clicks()
    {
        return $this->hasMany(Click::class);
    }

    public function getTotalclicksAttribute()
    {
        return $this->clicks()->sum('click_count');
    }

    public function getCreatedAtFormattedAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->locale('fr')->isoFormat('LLLL');
    }
}
