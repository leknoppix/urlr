<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @mixin IdeHelperClick
 */
class Click extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function url()
    {
        return $this->belongsTo(Url::class);
    }
}
