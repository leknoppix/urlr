<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * @mixin IdeHelperUser
 */
class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'surname',
        'email',
        'password',
    ];

    protected $guarded = [];

    public function urls()
    {
        return $this->hasMany(Url::class);
    }

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * Get the attributes that should be cast.
     *
     * @return array<string, string>
     */
    protected function casts(): array
    {
        return [
            'email_verified_at' => 'datetime',
            'password' => 'hashed',
        ];
    }

    protected $appends = ['created_at_formatted'];

    public function getCreatedAtFormattedAttribute()
    {
        $createdAt = $this->attributes['created_at'];

        // Vérifier si la valeur de created_at est déjà un objet Carbon
        if ($createdAt instanceof Carbon) {
            return $createdAt->locale('fr')->isoFormat('LLLL');
        }

        // Si ce n'est pas un objet Carbon, essayez de le parser
        if (is_string($createdAt)) {
            return Carbon::parse($createdAt)->locale('fr')->isoFormat('LLLL');
        }

        // Si la valeur n'est ni un objet Carbon ni une chaîne de caractères valide, retournez null ou une valeur par défaut
        return null; // ou une valeur par défaut, selon le cas
    }
}
