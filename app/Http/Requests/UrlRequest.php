<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UrlRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'slug' => [
                'required',
                'string',
                'max:15',
                Rule::unique('urls', 'slug')->ignore($this->route('url')),
            ],
            'url' => 'required|string|url|max:255',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'name.required' => __('Le champ "Nom du lien" est requis.', ['attribute' => __('Nom du lien')]),
            'name.string' => __('Le champ "Nom du lien" doit être une chaîne de caractères.', ['attribute' => __('Nom du lien')]),
            'name.max' => __('Le champ "Nom du lien" ne peut pas dépasser 255 caractères.', ['attribute' => __('Nom du lien')]),
            'slug.required' => __('Le champ "Slug pour l\'url" est requis.', ['attribute' => __('Slug')]),
            'slug.string' => __('Le champ "Slug pour l\'url" doit être une chaîne de caractères.', ['attribute' => __('Slug')]),
            'slug.max' => __('Le champ "Slug pour l\'url" ne peut pas dépasser 255 caractères.', ['attribute' => __('Slug')]),
            'slug.unique' => __('Ce champ "Slug pour l\'url" est déjà utilisé.', ['attribute' => __('Slug')]),
            'url.required' => __('Le champ "Adresse internet complète" est requis.', ['attribute' => __('URL')]),
            'url.string' => __('Le champ "Adresse internet complète" doit être une chaîne de caractères.', ['attribute' => __('URL')]),
            'url.url' => __('Le champ "Adresse internet complète" doit être une URL valide.', ['attribute' => __('URL')]),
            'url.max' => __('Le champ "Adresse internet complète" ne peut pas dépasser 255 caractères.', ['attribute' => __('URL')]),
        ];
    }
}
