<?php

namespace App\Http\Controllers;

use App\Models\Click;
use App\Models\Url;

class UrlController extends Controller
{
    public function index()
    {
        return redirect(config('url.url_redirect'));
    }

    public function redirection($slug)
    {
        try {
            $url = Url::where('slug', $slug)->firstOrFail();
            $this->countClick($url->id);

            return redirect($url->url);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return redirect(config('url.url_redirect'));
        }
    }

    protected function countClick($urlId)
    {
        $date = now()->toDateString();
        $click = Click::updateOrCreate(
            ['url_id' => $urlId, 'click_date' => $date],
            ['click_date' => $date]
        );
        // Incrémente click_count après que l'entrée a été récupérée ou créée
        $click->increment('click_count');
    }
}
