<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Click;
use App\Models\Url;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function __invoke(Request $request)
    {
        // nombre de liens générées
        $nb_links = Url::count();
        //nombre de clics total
        $nb_click = Click::sum('click_count');
        //moyenne des clics par lien
        $moyenne = round($nb_click / $nb_links, 2);

        return view('dashboard', compact('nb_links', 'nb_click', 'moyenne'));
    }
}
