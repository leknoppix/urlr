<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UrlRequest;
use App\Models\Url;
use App\Models\User;
use BeaconsBay\QrCode\Facades\QrCode;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;

class UrlController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $urls = Url::orderby('created_at', 'desc')->get();

        return view('admin.links.index', compact('urls'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //génération d'une proposition de slug aleatoire (priorisation lettres et chiffres)
        $slug = bin2hex(random_bytes(5));
        $url = new Url;
        $url->fill([
            'slug' => $slug,
        ]);

        return view('admin.links.create', [
            'url' => $url,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(UrlRequest $request)
    {
        $userId = auth()->user()->id;
        $validatedData = $request->validated();
        $validatedData['user_id'] = $userId;
        $url = Url::create($validatedData);
        $file = $this->createQrCode(Config::get('app.url') . '/' . $url->slug);
        $url->update(['fileurl' => $file]);

        return redirect()->route('administration.links.show', $url)->with('status', 'url-created');
    }

    public function downloadQrCode(Url $url)
    {
        $file = Storage::disk('qrcode')->path(md5(Config::get('app.url') . '/' . $url->slug) . '.png');

        return response()->download($file, 'QrCode ' . $url->name . '.png');
    }

    /**
     * Display the specified resource.
     */
    public function show(Url $url)
    {
        Carbon::setLocale('fr');
        $limit = 30;
        $dataclics = $url->clicks()
            ->latest()
            //->take($limit)
            ->select('click_date', 'click_count')
            ->get()
            ->map(function ($click) {
                $click->click_date = Carbon::parse($click->click_date)->format('d/m/Y');

                return $click;
            });
        $data = [
            'labels' => $dataclics->pluck('click_date')->all(),
            'data' => $dataclics->pluck('click_count')->all(),
        ];

        $user = User::select('name', 'surname', 'created_at')->find($url->user_id);
        if (is_null($user)) {
            $user = new User;
            $user->name = 'désactivé';
            $user->surname = 'Utilisateur';
            $user->created_at = $url->created_at;
        }

        return view('admin.links.view', compact('url', 'data', 'user'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Url $url)
    {
        return view('admin.links.edit', [
            'url' => $url,
            'isUpdating' => true,
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UrlRequest $request, Url $url)
    {
        $url->update($request->validated());

        return redirect()->route('administration.links.show', $url)->with('status', 'url-updated');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Url $url)
    {
        $file = Config::get('app.url') . '/' . $url->slug;

        Storage::disk('qrcode')->delete(md5($file) . '.png');

        $url->delete();

        return redirect()->route('administration.links.index')->with('status', 'url-delete');
    }

    private function createQrCode($url): string
    {
        $name = md5($url);
        $data = QrCode::size(4000)
            ->format('png')
            ->margin(1)
            ->merge('/storage/app/public/logo_mirande_HD.jpg', .3)
            ->errorCorrection('H')
            ->generate(
                $url,
            );
        Storage::disk('qrcode')->put($name . '.png', $data);

        return asset('qr_code/' . $name . '.png');
    }
}
