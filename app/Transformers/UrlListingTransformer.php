<?php

namespace App\Transformers;

use App\Models\Url;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class UrlListingTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array<string,string|int|null>
     */
    public function transform(Url $url)
    {
        return [
            'id' => $url->id,
            'slug' => $url->slug,
            'url' => $url->url,
            'user_name' => $url->user->name . ' ' . $url->user->surname,
            'created_at' => Carbon::parse($url->created_at)->isoFormat('DD MMMM Y'),
            'totalclicks' => $url->totalclicks,
        ];
    }
}
