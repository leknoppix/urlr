import './bootstrap';
import { confirmUrlDelete } from './url';
import ChartJs from 'chart.js/auto';


import Alpine from 'alpinejs';

window.Alpine = Alpine;

window.Chart = ChartJs;

window.confirmUrlDelete = confirmUrlDelete;

Alpine.start();