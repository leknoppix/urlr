<x-app-layout>
    <x-slot name="header">
        <div class="bg-white p-4 flex items-center flex-wrap">
            <ul class="flex items-center">
                <li class="inline-flex items-center">
                <span class="text-gray-600 hover:text-blue-500">Fil d'ariane : </span>
                <a href="{{ route('dashboard') }}" class="text-gray-600 hover:text-blue-500">
                    <svg class="w-5 h-auto fill-current mx-2 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#000000"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M10 19v-5h4v5c0 .55.45 1 1 1h3c.55 0 1-.45 1-1v-7h1.7c.46 0 .68-.57.33-.87L12.67 3.6c-.38-.34-.96-.34-1.34 0l-8.36 7.53c-.34.3-.13.87.33.87H5v7c0 .55.45 1 1 1h3c.55 0 1-.45 1-1z"/></svg>
                </a>
                <a href="{{ route('dashboard') }}" class="text-gray-600 hover:text-blue-500">
                    {{ __('Tableau de bord') }}
                </a>

                <span class="mx-4 h-auto text-gray-400 font-medium ml-2 mr-2">/</span>
                </li>

                <li class="inline-flex items-center">
                <a href="{{ route('administration.users.index') }}" class="text-gray-600 hover:text-blue-500">
                    {{ __('Listing des utilisateurs') }}
                </a>
                <span class="mx-4 h-auto text-gray-400 font-medium ml-2 mr-2">/</span>
                </li>
                <li class="inline-flex items-center">
                <a href="{{ route('administration.users.edit', $user) }}" class="text-gray-600 hover:text-blue-500">
                    {{ __('Modifier un utilisateur') }}
                </a>

                </li>
            </ul>
        </div>
    </x-slot>

    <div class="py-12 bg-white">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <section>
                        <form id="send-verification" method="post" action="{{ route('administration.users.update', $user) }}">
                            @method('PATCH')
                            @csrf
                            @include('admin.users.partials.forms')
                            <div class="flex justify-center items-center gap-4">
                                <x-primary-button>{{ __('Modifier cet utilisateur') }}</x-primary-button>
                                @if (session('status') === 'user-updated')
                                    <p
                                        x-data="{ show: true }"
                                        x-show="show"
                                        x-transition
                                        x-init="setTimeout(() => show = false, 5000)"
                                        class="text-sm text-gray-600"
                                    >{{ __('Modification prise en compte.') }}
                                    </p>
                                @endif
                            </div>
                        </form>
                    </section>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
