        <div class="flex w-full">
            <div class="w-1/2 divide-y divide-gray-200 m-4">
                <div class="py-1 text-base leading-6 space-y-2 text-gray-700 sm:text-lg sm:leading-7">
                    <div class="flex flex-col">
                        <x-input-label for="name" :value="__('Nom de l\'utilisateur')" />
                        <x-text-input id="name" name="name" type="text" class="mt-1 block w-full border-gray-500" :value="$user->name ?? old('name')" required autofocus autocomplete="name" />
                        <x-input-error class="mt-2" :messages="$errors->get('name')" />
                    </div>
                </div>
            </div>
            <div class="w-1/2 divide-y divide-gray-200 m-4">
                <div class="py-1 text-base leading-6 space-y-2 text-gray-700 sm:text-lg sm:leading-7">
                    <div class="flex flex-col">
                        <x-input-label for="surname" :value="__('Prénom de l\'utilisateur')" />
                        <x-text-input id="surname" name="surname" type="text" class="mt-1 block w-full border-gray-500" :value="$user->surname ?? old('surname')" required autofocus autocomplete="surname"/>
                        <x-input-error class="mt-2" :messages="$errors->get('surname')" />
                    </div>
                </div>
            </div>
        </div>
        <div class="w-full divide-y divide-gray-200 m-4">
            <div class="py-1 text-base leading-6 space-y-2 text-gray-700 sm:text-lg sm:leading-7">
                <div class="flex flex-col">
                    <x-input-label for="email" :value="__('Adresse mail de l\'utilisateur')" />
                    <x-text-input id="email" name="email" type="text" class="mt-1 block w-full border-gray-500" :value="$user->email ?? old('email')" required autofocus autocomplete="email" />
                    <x-input-error class="mt-2" :messages="$errors->get('email')" />
                </div>
            </div>
        </div>