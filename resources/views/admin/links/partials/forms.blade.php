        <div class="flex w-full">
            <div class="w-1/2 divide-y divide-gray-200 m-4">
                <div class="py-1 text-base leading-6 space-y-2 text-gray-700 sm:text-lg sm:leading-7">
                    <div class="flex flex-col">
                        <x-input-label for="name" :value="__('Nom du lien')" />
                        <x-text-input id="name" name="name" type="text" class="mt-1 block w-full border-gray-500" :value="$url->name ?? old('name')" required autofocus autocomplete="name" />
                        <x-input-error class="mt-2" :messages="$errors->get('name')" />
                    </div>
                </div>
            </div>
            <div class="w-1/2 divide-y divide-gray-200 m-4">
                <div class="py-1 text-base leading-6 space-y-2 text-gray-700 sm:text-lg sm:leading-7">
                    <div class="flex flex-col">
                        <x-input-label for="slug" :value="__('Slug pour l\'url (raccourci)')" />
                        <x-text-input id="slug" name="slug" type="text" class="mt-1 block w-full border-gray-500" :value="$url->slug ?? old('slug')" required autofocus autocomplete="slug" :readonly="$isUpdating ?? false"/>
                        <x-input-error class="mt-2" :messages="$errors->get('slug')" />
                    </div>
                </div>
            </div>
        </div>
        <div class="w-full divide-y divide-gray-200 m-4">
            <div class="py-1 text-base leading-6 space-y-2 text-gray-700 sm:text-lg sm:leading-7">
                <div class="flex flex-col">
                    <x-input-label for="url" :value="__('Adresse internet complète')" />
                    <x-text-input id="url" name="url" type="text" class="mt-1 block w-full border-gray-500" :value="$url->url ?? old('url')" required autofocus autocomplete="url" />
                    <x-input-error class="mt-2" :messages="$errors->get('url')" />
                </div>
            </div>
        </div>