<x-app-layout>
    <x-slot name="header">
        <div class="bg-white p-4 flex items-center flex-wrap">
            <ul class="flex items-center">
                <li class="inline-flex items-center">
                <span class="text-gray-600 hover:text-blue-500">Fil d'ariane : </span>
                <a href="{{ route('dashboard') }}" class="text-gray-600 hover:text-blue-500">
                    <svg class="w-5 h-auto fill-current mx-2 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#000000"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M10 19v-5h4v5c0 .55.45 1 1 1h3c.55 0 1-.45 1-1v-7h1.7c.46 0 .68-.57.33-.87L12.67 3.6c-.38-.34-.96-.34-1.34 0l-8.36 7.53c-.34.3-.13.87.33.87H5v7c0 .55.45 1 1 1h3c.55 0 1-.45 1-1z"/></svg>
                </a>
                <a href="{{ route('dashboard') }}" class="text-gray-600 hover:text-blue-500">
                    {{ __('Tableau de bord') }}
                </a>

                <span class="mx-4 h-auto text-gray-400 font-medium ml-2 mr-2">/</span>
                </li>

                <li class="inline-flex items-center">
                <a href="{{ route('administration.links.index') }}" class="text-gray-600 hover:text-blue-500">
                    {{ __('Listing des liens') }}
                </a>

                </li>
            </ul>
        </div>
    </x-slot>
    <div class="py-2 bg-white">
        <div class="flex items-center">
            <a href="{{ route('administration.links.create') }}" class="bg-green-200 rounded-lg font-normal text-gray-900 pl-4 pr-4 pt-1 pb-1 mx-auto">{{ __('Création d\'un nouveau lien') }}</a>
        </div>
    </div>
    <div class="py-2 bg-white">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    @include('admin.links.partials.notifications')
                    <table class="min-w-full text-left text-sm font-light text-surface dark:text-white">
                        <thead class="border-b border-r border-l border-t border-neutral-200 font-medium dark:border-white/10">
                            <tr>
                                <th class="border-e border-r border-b border-neutral-200 px-6 py-4 dark:border-white/10">Nom du lien</th>
                                <th class="border-e border-r border-b border-neutral-200 px-6 py-4 dark:border-white/10">Url initiale</th>
                                <th class="border-e border-r border-b border-neutral-200 px-6 py-4 dark:border-white/10">Url Shorten</th>
                                <th class="text-center border-e border-r border-b border-neutral-200 px-6 py-4 dark:border-white/10">Nombre de clicks</th>
                                <th class="text-center border-e border-r border-b border-neutral-200 px-6 py-4 dark:border-white/10">Action</th>
                            </tr>
                        </thead>
                        @foreach($urls as $url)
                            <tbody>
                                <tr class="border-l border-r border-b border-t border-neutral-200 dark:border-white/10">
                                    <td class="border-e border-r border-b border-neutral-200 px-6 py-4 dark:border-white/10">{{ $url['name'] }}</td>
                                    <td class="border-e border-r border-b border-neutral-200 px-6 py-4 dark:border-white/10">
                                        <a href="{{ $url->url }}" title="Lien vers l'url originelle {{ $url->name }}" target="_blank" class="text-blue-500">
                                            {{ $url['url'] }}
                                        </a>
                                    </td>
                                    <td class="border-e border-r border-b border-neutral-200 px-6 py-4 dark:border-white/10">
                                        <a href="{{ Config::get('app.url') }}/{{ $url->slug }}" title="Lien vers l'url raccourcie {{ $url->name }}"  target="_blank" class="text-blue-500">
                                            {{ Config::get('app.url') }}/{{ $url['slug'] }}
                                        </a>
                                    </td>
                                    <td class="text-center border-e border-r border-b border-neutral-200 px-6 py-4 dark:border-white/10">{{ $url['totalclicks'] }} clics</td>
                                    <td class="border-e border-r border-b border-neutral-200 px-6 py-4 dark:border-white/10">
                                        <div class="flex items-center">
                                            <a href="{{ route('administration.links.download', $url) }}" title="Télecharger le QR-Code de l'url {{ $url->name }}" class="ml-1 mr-1 transform transition duration-300 hover:scale-150 inline-block">
                                                <x-icon-qr class="w-8 h-8"/>
                                            </a>
                                            <a href="{{ route('administration.links.show', $url) }}" class="ml-1 mr-1 transform transition duration-300 hover:scale-150 inline-block">
                                                <x-icon-show class="w-8 h-8"/>
                                            </a>
                                            <a href="{{ route('administration.links.edit', $url) }}" class="ml-1 mr-1 transform transition duration-300 hover:scale-150 inline-block">
                                                <x-icon-edit class="w-8 h-8"/>
                                            </a>
                                            <form id="deleteForm_{{ $url['id'] }}" action="{{ route('administration.links.destroy', $url) }}" method="post" class="inline-block">
                                                @csrf
                                                @method('delete')
                                                <button onclick="return confirmUrlDelete({{ $url['id'] }})" class="ml-1 mr-1 transform transition duration-300 hover:scale-150"><x-icon-delete class="w-8 h-8"/></button>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

