<x-app-layout>
    <x-slot name="header">
        <div class="bg-white p-4 flex items-center flex-wrap">
            <ul class="flex items-center">
                <li class="inline-flex items-center">
                <span class="text-gray-600 hover:text-blue-500">Fil d'ariane : </span>
                <a href="{{ route('dashboard') }}" class="text-gray-600 hover:text-blue-500">
                    <svg class="w-5 h-auto fill-current mx-2 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#000000"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M10 19v-5h4v5c0 .55.45 1 1 1h3c.55 0 1-.45 1-1v-7h1.7c.46 0 .68-.57.33-.87L12.67 3.6c-.38-.34-.96-.34-1.34 0l-8.36 7.53c-.34.3-.13.87.33.87H5v7c0 .55.45 1 1 1h3c.55 0 1-.45 1-1z"/></svg>
                </a>
                <a href="{{ route('dashboard') }}" class="text-gray-600 hover:text-blue-500">
                    {{ __('Tableau de bord') }}
                </a>

                <span class="mx-4 h-auto text-gray-400 font-medium ml-2 mr-2">/</span>
                </li>

                <li class="inline-flex items-center">
                <a href="{{ route('administration.links.index') }}" class="text-gray-600 hover:text-blue-500">
                    {{ __('Listing des liens') }}
                </a>

                <span class="mx-4 h-auto text-gray-400 font-medium ml-2 mr-2">/</span>
                </li>

                <li class="inline-flex items-center">
                <a href="{{ route('administration.links.show', $url) }}" class="text-gray-600 hover:text-blue-500">
                    {{ __('Aperçu des informations sur le lien #') }}{{ $url->id }}
                </a>
                
                </span>
                </li>
            </ul>
        </div>
    </x-slot>
    <div class="py-2 bg-white">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    @include('admin.links.partials.notifications')
                    <div class="flex w-full">
                        <div class="w-1/3 divide-y divide-gray-200 m-4">
                            <div class="py-1 text-base leading-6 space-y-2 text-gray-700 sm:text-lg sm:leading-7">
                                <strong>Nom du lien : </strong> {{ $url->name }}
                            </div>
                            <div class="py-1 text-base leading-6 space-y-2 text-gray-700 sm:text-lg sm:leading-7">
                                <strong>Url originelle : </strong>
                                <br />
                                <a href="{{ $url->url }}" target="_blank" title="Lien vers l'url originelle {{ $url->name }}" class="text-blue-500">
                                    {{ $url->url }}
                                </a>
                            </div>
                            <div class="py-1 text-base leading-6 space-y-2 text-gray-700 sm:text-lg sm:leading-7">
                                <strong>Url raccourcie : </strong>
                                <br />
                                <a href="{{ Config::get('app.url') }}/{{ $url->slug }}" target="_blank" title="Lien vers l'url raccourcie {{ $url->name }}" class="text-blue-500">
                                    {{ Config::get('app.url') }}/{{ $url->slug }}
                                </a>
                            </div>
                            <div class="py-1 text-base leading-6 space-y-2 text-gray-700 sm:text-lg sm:leading-7">
                                <strong>Date de création : </strong> {{ $url->created_at_formatted }}
                            </div>
                            <div class="py-1 text-base leading-6 space-y-2 text-gray-700 sm:text-lg sm:leading-7">
                                <strong>Créé par : </strong> {{ $user->surname }} {{ $user->name }}
                            </div>
                            <div class="py-1 text-base leading-6 space-y-2 text-gray-700 sm:text-lg sm:leading-7">
                                <strong>Aperçu du QR-Code</strong>
                                <br />
                                <a href="{{ route('administration.links.download', $url) }}" title="Télecharger le QR-Code de l'url {{ $url->name }}" class="text-blue-500">
                                    <img src="{{ $url->fileurl }}" alt="QR-Code de l'url {{ $url->name }}" title="QR-Code de l'url {{ $url->name }}"/>
                                </a>
                            </div>
                        </div>
                        <div class="w-2/3 divide-y divide-gray-200 m-4">
                            <div class="py-1 text-base leading-6 space-y-2 text-gray-700 sm:text-lg sm:leading-7">
                                Nombre de clics par jour
                            </div>
                            <canvas id="barChart"></canvas>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

<script type="text/javascript">
document.addEventListener('DOMContentLoaded', function () {
    var ctx = document.getElementById('barChart').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: @json($data['labels']),
                datasets: [{
                    label: 'Nombre de clics',
                    data: @json($data['data']),
                    backgroundColor: 'rgba(75, 192, 192, 0.2)',
                    borderColor: 'rgba(75, 192, 192, 1)',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
    });
</script>