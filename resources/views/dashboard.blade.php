<x-app-layout>
    <x-slot name="header">
        <div class="bg-white p-4 flex items-center flex-wrap">
            <ul class="flex items-center">
                <li class="inline-flex items-center">
                <span class="text-gray-600 hover:text-blue-500">Fil d'ariane : </span>
                <a href="{{ route('dashboard') }}" class="text-gray-600 hover:text-blue-500">
                    <svg class="w-5 h-auto fill-current mx-2 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#000000"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M10 19v-5h4v5c0 .55.45 1 1 1h3c.55 0 1-.45 1-1v-7h1.7c.46 0 .68-.57.33-.87L12.67 3.6c-.38-.34-.96-.34-1.34 0l-8.36 7.53c-.34.3-.13.87.33.87H5v7c0 .55.45 1 1 1h3c.55 0 1-.45 1-1z"/></svg>
                </a>
                <a href="{{ route('dashboard') }}" class="text-gray-600 hover:text-blue-500">
                    {{ __('Tableau de bord') }}
                </a>
                </li>
            </ul>
        </div>
    </x-slot>

    <div class="py-12 bg-white">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    {{ __("Bienvenue sur votre tableau de bord: ". auth()->user()->name . " " . auth()->user()->surname) }}
                </div>
            </div>
        </div>
    </div>
    <div class="py-12 bg-white">
        <div class="flex flex-wrap -mx-4">
            <div class="relative w-full md:w-1/3 px-4 pb-9 md:pb-0 mb-12 lg:mb-0">
                <div class="hidden md:block absolute top-1/2 right-0 w-px h-28 bg-gray-200 transform -translate-y-1/2"></div>
                <div class="md:hidden absolute bottom-0 left-1/2 h-px w-40 bg-gray-200 transform -translate-x-1/2"></div>
                <div class="flex justify-center items-center">
                    <x-dashboard-link class="block text-5xl lg:text-7xl font-bold text-gray-900 mb-5"></x-dashboard-link>
                </div>
                <div class="text-center">
                    <span class="block text-5xl lg:text-7xl font-bold text-gray-900 mb-5">{{ $nb_links }}</span>
                    <span class="text-xl text-gray-500 uppercase">{{ __("Liens") }}</span>
                </div>
            </div>
            <div class="relative w-full md:w-1/3 px-4 pb-9 md:pb-0 mb-12 lg:mb-0">
                <div class="hidden md:block absolute top-1/2 right-0 w-px h-28 bg-gray-200 transform -translate-y-1/2"></div>
                <div class="md:hidden absolute bottom-0 left-1/2 h-px w-40 bg-gray-200 transform -translate-x-1/2"></div>
                <div class="flex justify-center items-center">
                    <x-dashboard-click class="block text-5xl lg:text-7xl font-bold text-gray-900 mb-5"></x-dashboard-click>
                </div>
                <div class="text-center">
                    <span class="block text-5xl lg:text-7xl font-bold text-gray-900 mb-5">{{ $nb_click}}</span>
                    <span class="text-xl text-gray-500 uppercase">{{ __("Clics") }}</span>
                </div>
            </div>
            <div class="w-full md:w-1/3 px-4">
                <div class="hidden md:block absolute top-1/2 right-0 w-px h-28 bg-gray-200 transform -translate-y-1/2"></div>
                <div class="md:hidden absolute bottom-0 left-1/2 h-px w-40 bg-gray-200 transform -translate-x-1/2"></div>
                <div class="flex justify-center items-center">
                    <x-dashboard-perlink class="block text-5xl lg:text-7xl font-bold text-gray-900 mb-5"></x-dashboard-perlink>
                </div>
                <div class="text-center">
                    <span class="block text-5xl lg:text-7xl font-bold text-gray-900 mb-5">{{ $moyenne }}</span>
                    <span class="text-xl text-gray-500 uppercase">{{ __("Clics en moyenne") }}</span>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
