<?php

namespace Database\Factories;

use App\Models\Url;
use App\Models\User;
use BeaconsBay\QrCode\Facades\QrCode;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Url>
 */
class UrlFactory extends Factory
{
    protected $model = Url::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $slug = Str::random(6);
        $this->createQrCode(Config::get('app.url') . '/' . $slug);

        return [
            'name' => $this->faker->name,
            'slug' => $slug,
            'url' => $this->faker->url,
            'user_id' => User::all()->random()->id,
            'fileurl' => $this->createQrCode(Config::get('app.url') . '/' . $slug),
        ];
    }

    private function createQrCode($url): string
    {
        $name = md5($url);
        $data = QrCode::size(2000)
            ->format('png')
            ->margin(1)
            ->merge('/storage/app/public/logo_mirande_HD.jpg', .3)
            ->errorCorrection('H')
            ->generate(
                $url,
            );
        Storage::disk('qrcode')->put($name . '.png', $data);

        return asset('qr_code/' . $name . '.png');
    }
}
