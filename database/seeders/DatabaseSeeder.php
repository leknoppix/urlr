<?php

namespace Database\Seeders;

use App\Models\Url;
use App\Models\User;
use Hash;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // User::factory(10)->create();

        User::factory()->create([
            'name' => 'Canadas',
            'surname' => 'Pascal',
            'email' => 'informatique@mirande.fr',
            'password' => Hash::make('admin'),
        ]);
        User::factory()->create([
            'name' => 'Castaing',
            'surname' => 'Manon',
            'email' => 'communication@mirande.fr',
            'password' => Hash::make('admin'),
        ]);
        // Création de 20 URLs
        Url::factory(20)->create();
        // Pour chaque URL, on crée des clics sur plusieurs jours (Clicks table date)
        Url::all()->each(function ($url) {
            // création d'une boucle pour avoir des dates à la suite basé sur X jours avant aujourd'hui
            $today = now();
            for ($i = 0; $i < rand(1, 500); $i++) {
                $date = $today->subDays($i);
                $url->clicks()->create([
                    'click_date' => $date,
                    'click_count' => rand(1, 10000),
                ]);
            }
            //
        });
    }
}
